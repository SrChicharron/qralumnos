import { StyleSheet, TextInput, View, Text } from 'react-native'
import React, {useEffect, useRef, useState } from 'react'
import { useContextPanelQRCode } from '../providers/QRCodeProvider'
import { Crypto, TipoAlgoritmoCripto } from '../utils/Crypto';

export default function PanelQRCode() {
    const [state, dispatch] = useContextPanelQRCode();
    const [selected, setSelected] = useState('#db643a')
    const [text, setText] = useState(state.text)
    const [typeCrypto, setTypeCrypto] = useState(TipoAlgoritmoCripto[1]);
    const colorPickerRef = useRef()
    const [timeRefreshQRCode, setTimeRefreshQRCode] = useState(5);

    const onDataChange = (value) => {
        setText(value);
        dispatch({ type: 'UPDATE_TEXT', text: value })
    }

    const onColorChange = (color) => {
        setSelected(color)
        dispatch({ type: 'UPDATE_COLOR', color: color })
    }

    const onValueChange = (value) => {
        dispatch({ type: 'UPDATE_SIZE', size: value })
    }

    const onEncryptText = async (itemTipoAlgoritmoCrypto) => {
        setTypeCrypto(itemTipoAlgoritmoCrypto)
        updateText();
    }
const updateText = async()=>{
  const crypto = Crypto(typeCrypto.algoritmo)
  const newText = text + new Date().getTime()
  const textoEncriptado = await crypto.encriptar(newText).then(textoEncriptado)
  dispatch({ type: "UPDATE_TEXT", text: textoEncriptado })
}

    useEffect(()=>{
      if(timeRefreshQRCode == 0) return;

      const mInterval = setInterval(updateText, timeRefreshQRCode * 1000);
      return () => clearInterval(mInterval);

    
    },[timeRefreshQRCode])

    return (
        <View >
            
        </View>
    )
}
