import React from 'react';
import { Image, View } from 'react-native';

export default function HeaderTitleWithImage () {
  return (
    <View style={{ flex: 0, justifyContent: 'center', alignItems: 'center', backgroundColor: '#ECF4D6' }}>
      <Image
      source={require('../assets/uteq.png')}
      style={{ width: 200, height: 40 }}
      resizeMode="contain"
    />
    </View>
  );
};