import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Login from './Login';
import Generar from './Generar';
import Escanear from './Escanear';
import HeaderTitleWithImage from '../components/HeaderTitleWithImage';
import { QRCodeProvider } from '../providers/QRCodeProvider';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

const Stack = createNativeStackNavigator();

export default function Page() {
    return (
        <View style={styles.main}>
            <QRCodeProvider>
            <Stack.Navigator
            screenOptions={{
                headerTitle: () => <HeaderTitleWithImage />,
                headerTitleAlign: 'center',
                headerStyle: {
                    backgroundColor: '#ECF4D6',
                  },
              }}
            >
            <Stack.Screen name="Login" component={Login}/>
            <Stack.Screen name="Generar" component={Generar} />
            <Stack.Screen name="Escanear" component={Escanear} />
            </Stack.Navigator>
            </QRCodeProvider>
        </View>
    );
}

const styles = StyleSheet.create({
    main: {
        height: '100%',
        justifyContent: 'space-evenly',
        padding: 20,
        backgroundColor: '#ECF4D6'
    },
})