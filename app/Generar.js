import { StyleSheet, Text, View, TextInput } from 'react-native';
import PanelQRCode from '../components/PanelQRCode';
import QRCode from '../components/QRCode';

export default function Generar() {

    return (
        <View style={styles.container}>
                <PanelQRCode />
                <QRCode />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ECF4D6',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
