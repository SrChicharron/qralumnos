import React, {useEffect, useRef, useState } from 'react'
import { useContextPanelQRCode } from '../providers/QRCodeProvider';
import { StyleSheet, TextInput, View, Text,TouchableOpacity } from 'react-native'
export default function Login({ navigation }) {
    const [userType1, setUserType1] = useState({ email: 'humberto.mendoza@uteq.edu.mx', password: 'password1234' });
    const [userType2, setUserType2] = useState({ email: '2015313073@uteq.edu.mx', password: 'password1234' });

    const [state, dispatch] = useContextPanelQRCode();
    const [mail, setMail] = useState("")
    const [pwd, setPwd] = useState("")

    function validarEmail() {
      // Expresión regular para validar el email de matrícula
      const regexMatricula = /^\d{10}@uteq\.edu.mx$/;
      // Expresión regular para validar el email de nombre de usuario
      const regexNombreUsuario = /^[a-zA-Z]+\.?[a-zA-Z]+@uteq\.edu.mx$/;
    
      if (regexMatricula.test(mail)) {
        console.log('Email de matrícula válido');
        if(mail===userType2.email&&pwd===userType2.password){
          dispatch({ type: 'UPDATE_TEXT', text: '2015313073' })
          navigation.navigate('Generar')
        }else{
          console.log("Credenciales incorrectas")
        }
      } else if (regexNombreUsuario.test(mail)) {
        if(mail===userType1.email&&pwd===userType1.password){
          navigation.navigate('Escanear')
        }else{
          console.log("Credenciales incorrectas")
        }
      } else {
        console.log('Email no válido');
      }
    }
        
      
    return (
      <View style={styles.container}>
        <Text style={styles.label}>Email</Text>
        <TextInput
          style={styles.input}
          value={mail}
          onChangeText={setMail}
          keyboardType="email-address"
          autoCapitalize="none"
        />
        <Text style={styles.label}>Password</Text>
        <TextInput
          style={styles.input}
          value={pwd}
          onChangeText={setPwd}
          secureTextEntry
        />
        <TouchableOpacity style={styles.button} onPress={validarEmail}>
          <Text style={styles.buttonText}>Login</Text>
        </TouchableOpacity>
      </View>
    );
  }
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      padding: 20,
      backgroundColor: '#ECF4D6', // Puedes cambiar este color si lo deseas
    },
    label: {
      fontSize: 16,
      fontWeight: 'bold',
      color: '#333',
      marginBottom: 5,
    },
    input: {
      width: '100%',
      height: 40,
      borderColor: '#ddd',
      borderWidth: 1,
      padding: 10,
      marginBottom: 20,
      borderRadius: 5,
      backgroundColor: '#fff',
    },
    button: {
      width: '100%',
      backgroundColor: '#007bff',
      padding: 10,
      borderRadius: 5,
      alignItems: 'center',
    },
    buttonText: {
      color: '#fff',
      fontSize: 18,
    },
  });